import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomerService } from '../../services/customer.service';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Customer } from '../../models/customer';

/********************************
 *
 * This component will show a table of customers along wth actions to display customer details.
 *
 *************************************************/

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  allCustomers: MatTableDataSource<any> = new MatTableDataSource();

  constructor(
    private customerService: CustomerService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getCustomers();
  }

  getCustomers() {
    let customerSubscription = this.customerService.getCustomers().subscribe(
      (data) => {
        console.log(data[2]);
        this.allCustomers.data = data;
      },
      (error) => {
        console.log(error);
      },
      () => {
        console.log('Subscription Completed.');
        customerSubscription.unsubscribe();
      }
    );
  }

  showCustomerDetails(customer: Customer) {
    this.customerService.setCustomerData(this.allCustomers.data);
    this.router.navigate(['customerDetails', { id: customer.id }]);
  }

  public doFilter = (value: string) => {
    this.allCustomers.filter = value.trim().toLocaleLowerCase();
  };
}
