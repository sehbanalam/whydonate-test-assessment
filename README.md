# WhyDonate Test Assessment (Sehban Alam)

- This project was generated with [Angular CLI](https://github.com/angular/angular-cli)
- This project is available at BitBucket. URL: (https://sehbanalam@bitbucket.org/sehbanalam/whydonate-test-assessment.git)
- This project is running live at firebase cloud hosting. URL: (https://whydonate-89b1b.web.app)

## Description

- This project contains

1. Angular Material Module

- It contains material design themes and components.

2. Features Module

- It contains the following:
  1. Components:
  - Home: To show list of customers.
  - Customer Details: To show customer details by filtering the data received from server.
  - Add Customer: To add a new customers
  2. Services:
  - Customer Service: To hold all the async functions and http calls.
  3. Models:
  - To hold and securely share data between components.

3. This project uses Bootstrap 5 css library.

4. Karma tests has been added using karma.conf.js file in root folder.

## Running

1. Clone the project from ButBucket repository. URL: (https://sehbanalam@bitbucket.org/sehbanalam/whydonate-test-assessment.git)
2. Run `npm install` after cloning this project.
3. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
