import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Customer } from '../../models/customer';
import { CustomerService } from '../../services/customer.service';

/********************************
 *
 * This component will show a details of a single customer.
 * It will fetch customer id from route.
 * It will search for the required customer using id. 
 *
 *************************************************/

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css'],
})
export class CustomerDetailsComponent implements OnInit {
  customerId: string;
  customerData: Customer[];
  selectedCustomer: any;

  constructor(
    private customerService: CustomerService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.customerId = this.activatedRoute.snapshot.paramMap.get('id');
    this.customerData = this.customerService.getCustomerData();

    if (!this.customerId || !this.customerData) {
      this.router.navigate(['home']);
    } else {
      this.getCustomerById();
    }
  }

  getCustomerById() {
    this.selectedCustomer = this.customerData.filter((customer) => {
      return (
        customer.id.toLocaleLowerCase() == this.customerId.toLocaleLowerCase()
      );
    });
  }
}
