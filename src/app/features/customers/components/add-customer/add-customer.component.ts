import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from '../../models/customer';
import { CustomerService } from '../../services/customer.service';

/********************************
 *
 * This component will show a form to add a new customer
 * It will submit the customer to service function and display the response from service
 *
 *************************************************/

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css'],
})
export class AddCustomerComponent implements OnInit {
  
  newCustomer: Customer;

  constructor(
    private router: Router,
    private customerService: CustomerService
  ) {}

  ngOnInit(): void {
    // Defining blank object
    this.newCustomer = new Customer('', '', '', '');
  }

  submitCustomer() {
    console.log(this.newCustomer);

    const addCustomerSubscription = this.customerService
      .addCustomer(this.newCustomer)
      .subscribe(
        (data) => {
          window.alert('Customer added successfully.');
        },
        (error) => {
          window.alert(
            'There was an error while adding a customer. Please try again later.'
          );
        },
        () => {
          addCustomerSubscription.unsubscribe();
          this.router.navigate(['home']);
        }
      );
  }
}
