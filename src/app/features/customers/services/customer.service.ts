import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Customer } from '../models/customer';

/**************************************************
 *
 * This service will take care of all async and long operations of customer module.
 *
 *************************************************/

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  constructor(private httpClient: HttpClient) {}

  /*******************************************
   * Object to hold data
   ********************************************/
  customerData: Customer[] = [];

  /*******************************************
   * Function retrieve all clients from API
   ********************************************/
  getCustomers() {
    const url = ' https://customerdemoapi.herokuapp.com/api/customer/';
    return this.httpClient.get<Customer[]>(url);
  }

  /*******************************************
   * Function to add a new customer
   ********************************************/
  addCustomer(customer: Customer) {
    const url = 'https://customerdemoapi.herokuapp.com/api/customer/';
    return this.httpClient.post(url, customer);
  }

  /*******************************************
   * Getters and Setters to share data
   ********************************************/
  getCustomerData() {
    return this.customerData;
  }

  setCustomerData(customerData: Customer[]) {
    this.customerData = customerData;
  }
}
