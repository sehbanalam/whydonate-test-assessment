/***********************************************************
 * Class that represent model of a CUSTOMER
 ***********************************************************/
export class Customer {
  id: string;
  name: string;
  email: string;
  phone: string;
  address: string;

  constructor(name: string, email: string, phone: string, address: string) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.address = address;
  }
}
